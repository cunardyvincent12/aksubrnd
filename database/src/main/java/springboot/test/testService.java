package springboot.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class testService {
	
	@Autowired
	private testService testRepository;
	
	private List<test> tests = new ArrayList<>(Arrays.asList(
			new test("coba","mencoba saja", "mungkin berjalan"),
			new test("vincent","mencobaspring","mungkin bisa spring"),
			new test("belajar","memahami","mencoba belajar untuk memahami")
			
			)); 
	public List<test> testall(){
//		return ;
		List<test> tests = new ArrayList<>();
		testRepository.findAll()
		.forEach(tests::add);
	}
	public test getTest(String id) {
		return tests.stream().filter(t -> t.getId().equals(id)).findFirst().get();
	}
	public void postTest(test Test) {
		tests.add(Test);
		
	}
	public void updateTest(String id, test Test) {
		for(int i = 0 ; i < tests.size(); i++) {
			test t = tests.get(i);
			if(t.getId().equals(id)) {
				tests.set(i,  Test);
				return;
			}
		}
	}
	public void deleteTest(String id) {
		tests.removeIf(t -> t.getId().equals(id));
	}
}
